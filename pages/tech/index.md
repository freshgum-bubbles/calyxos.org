---
title: Technical Details
nav_title: Tech
---

The tech behind CalyxOS.

<br>

## [Datura](datura)
The Firewall app.

## [microG](microg)
A free-as-in-freedom re-implementation of Google’s proprietary Android user space apps and libraries.