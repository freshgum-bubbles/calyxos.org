---
title: F‐Droid
---
### F-Droid

* Prebuilt

### F-Droid Priveleged Extension

* Could be changed to prebuilt once F-Droid is set to `PRESIGNED`

### Local repo - https://gitlab.com/calyxos/calyxos/issues/5

* A local repo has been created to help with the initial app install during SetupWizard

TODO: Figure out the exact semantics here
* Ideally, it would be a partial mirror
* The apps included currently are either from the F-Droid repo, or from the Guardian Project repo, as-is, prebuilt, presigned.
* Only outlier is Signal, apk grabbed from the website.
* Should include some basic info about apps, right now all that's included is a barebones index + the apks themselves, no icons either.

### Calyx repo - https://gitlab.com/calyxos/calyxos/issues/7

* To include apps such as Chromium.