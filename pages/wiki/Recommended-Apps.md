---
title: Recommended-Apps
---
This page is for tracking apps that we want to recommend for CalyxOS.

Criteria:

* Free
* Actively developed
* Don't suck
* Useful

## Apps

* Apps already included in the OS are marked **bold**

Stores

* **F-Droid**
* **Aurora Store**
* Aurora Droid?

Networking

* **RiseupVPN**
* [**CalyxVPN**](https://f-droid.org/en/packages/org.calyxinstitute.vpn/)
* **Orbot** (Tor)
* **Tor Browser**

Communication

* **Signal**
* Jitsi Meet
* DeltaChat
* Simple Contacts
* **Briar**
* **Conversations**

Productivity

* Etar Calendar

Utility

* **Privacy Friendly Weather**
* Voice Recorder
* **MuPDF Viewer**
* KeyChain
* [OpenBoard](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/)
* [Material Files](https://f-droid.org/en/packages/me.zhanghai.android.files/)

Maps and Location

* OsmAnd+

Backup and Storage

* Nextcloud
* **SeedVault**

Photos and Video

* **Scrambled EXIF**
* [LineageOS Gallery2](https://github.com/LineageOS/android_packages_apps_Gallery2)
* VLC

## Links

* https://gitlab.com/calyxos/calyxos-fdroid-repo/-/tree/master/fdroid/metadata
* https://simplemobiletools.github.io/
* https://github.com/SecUSo Security Usability Society
* https://old.reddit.com/r/privacy/comments/g2p1e0/best_of_fdroid_2020/
